IRIS PIC
========

The PIC format is a proprietary image format created for Christian Buil's IRIS 
software. To ensure compatibility with the latter, Siril is able to read this 
type of file. However, because the format is proprietary and the specifications 
are not known, not all header information will be saved when converting to FITS.

Siril cannot record in PIC format.
