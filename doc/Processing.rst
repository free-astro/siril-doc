Processing
==========

This section takes you through the different processing steps of your images. 
The drop-down menu is accessible from the header bar using the 
:guilabel:`Image Processing` button. The tools are grouped in the menus and 
sub-menus, and in this documentation too, by theme.

.. figure:: _images/processing/processing_menu.png
    :alt: processing menu
    :class: with-shadow

    Image processing menu

.. toctree::
   :hidden:

   processing/stretching
   processing/color_calibritation
   processing/colors
   processing/filters
   processing/stars
   processing/geometry
   processing/background
   processing/graxpert
   processing/extraction
   processing/lmatch
   processing/rgbcomp
   processing/merge
   processing/pixelmath


