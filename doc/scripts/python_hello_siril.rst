Hello, Siril!
#############
It is traditional to start with a Hello, World! example, so here is
"Hello, Siril!"

.. code-block:: python

   import sirilpy as s
   siril = s.SirilInterface()
   if siril.connect():
      siril.log("Hello, Siril!")
   siril.disconnect()

That's it. All siril python scripts should import the siril module: this
is automatically provided by Siril and does not require installation.

It allows connection to Siril through the **SirilInterface** class and
gives access to Siril methods: in this case the ``log()`` function does a
tiny bit of adjustment (adding a ``\n`` newline character) and calls the
internal function ``siril_log_message()`` to display the result in Siril's
log tab.

Note that the ``siril.disconnect()`` call is not really essential: at the
end of a script the pipe or socket used for communication will be closed
anyway as the python process quits.
