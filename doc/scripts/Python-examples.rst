Python Script Examples
======================

This section highlights examples of python scripting. These are
simple examples intended to provide an introduction to the API,
and are explained with commentary.

For a more complete example of a script using a TKinter interface
to provide a full-featured interface to an external program, see
the CosmicClarity_denoise.py and CosmicClarity_sharpen.py scripts
which are available in the Siril scripts repository.

.. figure:: ../_images/scripts/Scripts_menu.png
    :alt: script menu
    :class: with-shadow

    Scripts menu

.. toctree::
   :hidden:

   python_hello_siril
   python_hello_astropy
   python_direct_interface
   python_plot
