Python Script Key Information
=============================

End Users
---------
End users should take care to obtain scripts from reputable sources.

* A few fundamentally important scripts are distributed as part of the core Siril package
  and can be found installed in the system data directory. These are tested to the
  same degree as the rest of Siril and the development team will treat bug reports
  on these scripts in exactly the same way as bug reports on any other part of Siril.
* 1.4.0 introduces the Scripts repository. Anyone may submit scripts to this repository
  by making an account on gitlab and submitting a merge request. The Siril team
  will provide a basic level of scrutiny that the scripts are not attempting to do
  anything malicious, but we do not accept any responsibility for the correct
  functionality of scripts written by other people and will not provide support for
  them - the author should be contacted directly.
* Script authors may choose to distribute scripts independently, as has been done in
  the past. This is fine: scripts can be downloaded and added to a script directory or
  run via the script editor from anywhere on the filesystem. In this case the Siril
  team have nothing at all to do with the scripts and cannot even assure you that they
  will do no harm, so ensure you trust the author.

Script Authors
--------------
We welcome all script authors who wish to write scripts for Siril. However supporting
Siril, while fun, takes a lot of time and we have neither the time nor the desire to
support other people's work as well as the core Siril software. There are therefore
some guidelines to ensure that the division of responsibility is clear.

* All scripts should feature the author's name or handle, as well as a means of
  contact (website, YouTube channel, forum etc.) where they can be reached with
  comments or bug reports. For authors publishing independently this is of course
  guidance, however for authors wishing to publish their scripts via the siril-scripts
  repository this is mandatory. Scripts will not be accepted unless these details
  are included.
* Only open-source scripts will be accepted in the Siril scripts repository.
  Closed source scripts (including ones that feature a source code wrapper that calls
  a closed source program) must be independently published.
* Authors should also follow the script programming guidelines on the API page.
  These guidelines ensure that scripts can be used by end-users with minimal
  friction.

Siril-Scripts Repository Code Standards
---------------------------------------
As a script author you can write whatever scripts you like for your own use. However
for scripts to be published to the siril-scripts repository we need to require some
coding standards in order to ensure that they work with the standard Siril packages
on each of the three supported operating systems. The requirements are:

* Scripts should work with versions of Python >= 3.9. This version matches the oldest
  Python version in our binary packages, and will be adjusted over time to match the
  packages.
* Scripts should be single-file scripts. The repository was originally designed for
  legacy .SSF scripts and the assumption of one file per script remains. We understand
  that it can be good for code readability to develop complex python programs by
  splitting them into multiple files. This is fine when working locally, however the
  multiple files would need to be combined into a single file for submission to the
  scripts repository. *Potentially in the future this decision may be revisited,*
  *however the 1 file per script limitation will apply for at least the 1.4 stable*
  *series of Siril.*
* If you want to implement a GUI in your script you should use the TKinter toolkit.
  This is a standard Python toolkit and works everywhere. The default look and feel
  is a bit "old school" but it can (and should) be modernised and aligned as far as
  possible with the Siril look and feel by importing ``ttkthemes``: examples are
  available in some of the Python scripts in the repository.

  .. warning::
     Scripts using GTK or Qt GUIs will not be accepted into the scripts repository.
     Each of these GUI frameworks requires substantial system dependencies. Although
     Siril itself is coded in GTK, unfortunately the python gi packages required to
     use it in python scripts are broken on Windows and we are unable to bundle them.
     In the case of Qt, it is simply the case that the dependencies we would need to
     bundle are excessive just for supporting some script GUIs, and would greatly
     increase the download size. If you wish, you can install the dependencies
     yourself and write scripts using GTK or Qt and they will work fine on your
     machine, but if you wish to distribute such scripts please see the section on
     system dependencies below.

  .. tip::
     In order to make it easier to write TKinter GUIs, a variety of drag and drop
     TKinter GUI designers are available via your favourite search engine. There is
     even the `TKedit<https://tkedit.glitch.me/>`_ browser-based GUI designer.

* Avoid import version specifications that specify a package version using "<", "<="
  or "==". These constraints will result in conflicts between scripts, which all
  share the same venv. Specifying a package version using ">=" is fine. Remember to
  use ``sirilpy.ensure_installed()`` before importing non-core python module
  dependencies, to automate the process of installing them.
* Avoid importing packages that require system packages (i.e. ones that cannot be
  installed using ``python3 -m pip install``)
* Ideally, check that your script imports will install on all three target operating
  systems. If not then the development team will check them after submission, and
  you will need to fix any issues that occur.

Known Good Modules
------------------
A list of "known good" packages that we have checked will import without problem, and
with all their dependencies, on all the target operating systems, is provided below:

* astropy
* astropy-healpix
* astroquery
* ccdproc
* GaiaXPy
* matplotlib
* numpy
* opencv-python
* pandas
* photutils
* pillow
* pyfftw
* pygaia
* scipy
* tk
* ttkthemes
*

Excluded Modules
----------------
Completely avoid using the following modules, as they are known to cause problems on one
or more OSes:

* healpy (doesn't work on Windows; astropy_healpix can be used instead)
*

Modules with System Dependencies
--------------------------------
The following modules require installation of system packages which cannot be automated
using pip, and therefore should not be directly used in Siril scripts:

* gi (pygtk etc - requires system package installation)
* pyqt (requires system package installation)
* pycuda and similar ML packages (require system package installation)
*

Workaround for Modules with System Dependencies
-----------------------------------------------
It is still possible to write projects that use these kind of packages, but you will
need to package them with their dependencies and distribute them independently of
Siril or the scripts repository, and the sirilpy module can still be used to achieve
integration with Siril. If you want to, you can provide a wrapper script that can be
distributed in the scripts repository and just initializes your main program.

Closed Source Scripts
---------------------
The python interface will happily run precompiled .pyc files. Siril is a Free and
Open-Source software, and we therefore do not encourage development of closed-source
scripts, however we don't forbid it either. Practically we can't, because even
stringent interpretations of the GNU Public License allow for writing an open source
shim that sits between Siril and a closed source application. Moreover we are in
favour of freedom, and while we choose to release Siril under the GPL to provide
freedom to our users, we also respect the freedom of developers to choose how they
release their own work.

There is some history here, too: Siril has since 1.2.0 provided a built-in interface
to Starnet, however Starnet was originally open source and only closed its doors
later on.

However, if an author chooses to release a Siril script as a closed-source .pyc then
they need to arrange all matters to do with distributing it: as we are unable to
inspect the code ourselves to perform even the most cursory checks, we cannot
host it in the Siril-scripts git repository. And of course, responsibility for
supporting such products is entirely a matter for the author - the Siril team will
offer no advice on such products.

