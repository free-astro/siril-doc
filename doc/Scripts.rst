Scripting
=========

This section explains the different scripting and automation
methods available in Siril.

.. tip::
   Python scripting was introduced in the 1.3.5 development version. It is
   currently marked as **EXPERIMENTAL**. This doesn't mean it will eat your
   data: the interface itself is robust and has been tested through
   development: the present experimental nature of it is more to do with the
   fact that we don't yet know what users will do with this new capability
   and whether there may be issues or limitations that we have not foreseen,
   perhaps due to the constraints of packaging or consistency across the
   different operating systems.

   Please try it out, either as a user by using scripts published on the
   scripts repository, or as a script writer. We welcome all your feedback
   and will aim to refine the interface throughout the 1.4 stable series and
   1.5 development series.

   If you want to debug your Python scripts at runtime, tick the box next to 
   :guilabel:`Enable Python debug mode`. A tutorial detailing the steps to attach
   to the Python process is shown in `Siril tutorial page <https://siril.org/tutorials/python_debug/>`_.

.. figure:: _images/scripts/Scripts_menu.png
    :alt: script menu
    :class: with-shadow
    :name: script menu

    Scripts menu

.. toctree::
   :hidden:

   scripts/Script-files
   scripts/Python-scripts
   scripts/pykeyinfo
   scripts/editor
   scripts/Python-examples

.. warning::
   **Not all scripts are written by, or the responsibility of, the Siril development team!**

   With the introduction of Python scripting the power available to Siril script
   writers has increased enormously. With great power comes great responsibility!

   :emphasis:`You`, as the user, need to ensure you trust the authors of scripts you use.
   1.4.0 introduces the script repository. Some of the scripts there have been
   written by the Siril development team but others have been written by
   third party contributors. Please read the Key Info section regarding guidance for
   script authors, end users and the rules for the script repository.
