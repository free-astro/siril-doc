GraXpert Interface
==================

Siril provides an interface to the external `GraXpert <https://graxpert.com>`_
tool. This is a third party piece of Free and Open Source software that provides 
sophisticated background removal and denoising routines. Although Siril's 
built-in background extraction uses a selected GraXpert algorithm, Siril 
currently does not directly support AI inferencing. This is because of the 
complexities of cross-platform GPU support and the fact that Siril is written in 
C, whereas almost all AI code uses python. Integration with GraXpert therefore 
provides a way to bring advanced AI astronomical image processing functionality 
to Siril users in a seamless manner.

In order to be used, the path to the working GraXpert installation must be set in
:menuselection:`Preferences->Miscellaneous`.

.. warning::
   If you are wondering **why GraXpert doesn’t launch**, please run it outside
   Siril first. It’s not Siril’s fault if it’s not supported by your computer or
   badly installed for some reason. Ensure you can correctly open GraXpert using
   its own GUI and that it works correctly by itself. If this is assured, then
   it will work when correctly configured in Siril.

Background Extraction
~~~~~~~~~~~~~~~~~~~~~

The GraXpert background extraction interface works in a very similar way to the
built-in one. In AI mode there is not even any need to generate a background
sampling grid: it is just necessary to select the AI algorithm (which is the
interface default anyway) and click :guilabel:`Apply`.

.. figure:: ../_images/processing/graxpert_bg_2.png
   :alt: dialog
   :class: with-shadow

   GraXpert interface, showing background extraction options.

In the other modes (RBF, Kriging and Spline) it is necessary to set a background
sampling grid. This works in exactly the same way as setting the grid for the
built-in background extraction tool (because it uses the same code). Samples can
be automatically placed by providing a density (:guilabel:`Samples per row`) and
clicking on :guilabel:`Generate`. If areas of the image are brighter than
the median by some factor :guilabel:`Grid tolerance` times sigma, then no sample 
will be placed there. Note that to achieve commonality with GraXpert's own UI
the Siril interface will allow setting the grid tolerance as low as -2.0 and up
to +6.0.

After generation, samples can also be added manually (left click) or removed
manually (right click).

Unlike the built-in background extraction algorithm it is not possible to
preview the background model, as it is calculated using external software, but
there is an option :guilabel:`Keep background` which will save the background
model, as computed by GraXpert, for subsequent inspection if required.

For additional details of how GraXpert background extraction works, see the
`website <https://www.graxpert.com>`_.
   
.. Deconvolution
.. ~~~~~~~~~~~~~
.. From version 3.1 onwards, GraXpert supports deconvolution. Two modes are possible:

.. - object deconvolution,
.. - stellar deconvolution.

.. These modes can be selected using the switch at top left.

.. To use deconvolution, simply select :guilabel:`Deconvolution` in the upper tab. 
.. This operation has two parameters, :guilabel:`Strength`, which takes values 
.. between 0.0 (no deconvolution) and 1.0 (maximum deconvolution) and 
.. :guilabel:`Image FWHM (in pixels)`, which also takes values between 0.0 
.. (prioritises fine structure retention) and 14.0 (prioritise deconvolution 
.. ability). If you set it to zero you can see a very (too) small effect on 
.. processed image, if so set it higher.

.. .. figure:: ../_images/processing/graxpert_deconv.png
..    :alt: dialog
..    :class: with-shadow

..    GraXpert interface, showing deconvolution options.
   
Denoising
~~~~~~~~~

To use GraXpert for denoising, select :guilabel:`Denoising` in the tab. 
This operation has only one parameter, :guilabel:`Strength`, which 
takes values between 0.0 (no denoising) and 1.0 (maximum denoising).

.. figure:: ../_images/processing/graxpert_noise_2.png
   :alt: dialog
   :class: with-shadow

   GraXpert interface, showing denoising options.

General settings
~~~~~~~~~~~~~~~~

* **Use GPU**: This option tells GraXpert to use the GPU for AI inferencing, if
  one is available. It is safe to leave this set, as GraXpert will fall back to
  CPU if no GPU is available. Note that the background extraction AI model is
  much quicker to run and the AI inferencing has a significant set-up time, so
  for background extraction you may find that CPU inferencing is marginally
  quicker. The denoising algorithm is significantly slower and it is always worth
  using AI inferencing for this if possible

* **AI batch size**: This option sets how many chunks GraXpert will attempt to
  run inferencing on in parallel. Note that setting this too high may cause
  out-of-memory errors, and may or may not provide a speed benefit dependent on
  your hardware.

* **Apply to Sequence**: Using GraXpert via the Siril interface provides a unique
  option that is not even available using GraXpert stand-alone: the ability to apply
  GraXpert processing to sequences of images. In order to achieve this, simply
  select the :guilabel:`Apply to sequence` check box. The sequence will be
  saved with the prefix `graxpert_`.

.. admonition:: Siril command line
   :class: sirilcommand
  
   .. include:: ../commands/graxpert_bg_use.rst

   .. include:: ../commands/graxpert_bg.rst

.. admonition:: Siril command line
   :class: sirilcommand

   .. include:: ../commands/graxpert_denoise_use.rst

   .. include:: ../commands/graxpert_denoise.rst

.. admonition:: Siril command line
   :class: sirilcommand

   .. include:: ../commands/seqgraxpert_bg_use.rst

   .. include:: ../commands/seqgraxpert_bg.rst

.. admonition:: Siril command line
   :class: sirilcommand

   .. include:: ../commands/seqgraxpert_denoise_use.rst

   .. include:: ../commands/seqgraxpert_denoise.rst
