.. |indexlink| image:: ./_images/icons/index.svg
               :target: std-cmdindex.html
               :alt: Back to index
               :width: 20px
.. |scriptable| image:: ./_images/icons/scriptable.svg
               :alt: Scriptable
.. |nonscriptable| image:: ./_images/icons/nonscriptable.svg
               :alt: Non scriptable

Commands
========

The following page lists all the commands available in Siril |release|.

You can access an index by clicking this icon |indexlink|.

Commands marked with this icon |scriptable| can be used in scripts while those marked with this one |nonscriptable| cannot.

.. tip::
   For all the sequence commands typed in the command bar of the GUI, you can replace argument **sequencename** with a ``.`` if the sequence to be processed is already loaded.
.. tip::
   If you want to provide an argument that includes a string with spaces, for example a filename, you need to quote the entire argument not just the string. So for example you should use ``command "-filename=My File.fits"``, **not** ``command -filename="My File.fits"``.
.. command:: addmax
   :scriptable: 0


.. include:: ./commands/addmax_use.rst


.. include:: ./commands/addmax.rst

| 

.. command:: asinh
   :scriptable: 1


.. include:: ./commands/asinh_use.rst


.. include:: ./commands/asinh.rst

| 

.. command:: autoghs
   :scriptable: 1


.. include:: ./commands/autoghs_use.rst


.. include:: ./commands/autoghs.rst

| 

.. command:: autostretch
   :scriptable: 1


.. include:: ./commands/autostretch_use.rst


.. include:: ./commands/autostretch.rst

| 

.. command:: bg
   :scriptable: 1


.. include:: ./commands/bg_use.rst


.. include:: ./commands/bg.rst

| 

.. command:: bgnoise
   :scriptable: 1


.. include:: ./commands/bgnoise_use.rst


.. include:: ./commands/bgnoise.rst

.. include:: ./commands/bgnoise_add.rst

| 

.. command:: binxy
   :scriptable: 1


.. include:: ./commands/binxy_use.rst


.. include:: ./commands/binxy.rst

| 

.. command:: boxselect
   :scriptable: 1


.. include:: ./commands/boxselect_use.rst


.. include:: ./commands/boxselect.rst

| 

.. command:: calibrate
   :scriptable: 1


.. include:: ./commands/calibrate_use.rst


.. include:: ./commands/calibrate.rst

| 

.. command:: calibrate_single
   :scriptable: 1


.. include:: ./commands/calibrate_single_use.rst


.. include:: ./commands/calibrate_single.rst

| 

.. command:: capabilities
   :scriptable: 1


.. include:: ./commands/capabilities_use.rst


.. include:: ./commands/capabilities.rst

| 

.. command:: catsearch
   :scriptable: 1


.. include:: ./commands/catsearch_use.rst


.. include:: ./commands/catsearch.rst

| 

.. command:: ccm
   :scriptable: 1


.. include:: ./commands/ccm_use.rst


.. include:: ./commands/ccm.rst

| 

.. command:: cd
   :scriptable: 1


.. include:: ./commands/cd_use.rst


.. include:: ./commands/cd.rst

| 

.. command:: cdg
   :scriptable: 1


.. include:: ./commands/cdg_use.rst


.. include:: ./commands/cdg.rst

| 

.. command:: clahe
   :scriptable: 1


.. include:: ./commands/clahe_use.rst


.. include:: ./commands/clahe.rst

| 

.. command:: clear
   :scriptable: 0


.. include:: ./commands/clear_use.rst


.. include:: ./commands/clear.rst

| 

.. command:: clearstar
   :scriptable: 0


.. include:: ./commands/clearstar_use.rst


.. include:: ./commands/clearstar.rst

| 

.. command:: close
   :scriptable: 1


.. include:: ./commands/close_use.rst


.. include:: ./commands/close.rst

| 

.. command:: conesearch
   :scriptable: 1


.. include:: ./commands/conesearch_use.rst


.. include:: ./commands/conesearch.rst

| 

.. command:: convert
   :scriptable: 1


.. include:: ./commands/convert_use.rst


.. include:: ./commands/convert.rst

| 

.. command:: convertraw
   :scriptable: 1


.. include:: ./commands/convertraw_use.rst


.. include:: ./commands/convertraw.rst

| 

.. command:: cosme
   :scriptable: 1


.. include:: ./commands/cosme_use.rst


.. include:: ./commands/cosme.rst

.. include:: ./commands/cosme_add.rst

| 

.. command:: cosme_cfa
   :scriptable: 1


.. include:: ./commands/cosme_cfa_use.rst


.. include:: ./commands/cosme_cfa.rst

| 

.. command:: crop
   :scriptable: 1


.. include:: ./commands/crop_use.rst


.. include:: ./commands/crop.rst

| 

.. command:: ddp
   :scriptable: 0


.. include:: ./commands/ddp_use.rst


.. include:: ./commands/ddp.rst

| 

.. command:: denoise
   :scriptable: 1


.. include:: ./commands/denoise_use.rst


.. include:: ./commands/denoise.rst

| 

.. command:: dir
   :scriptable: 0


.. include:: ./commands/dir_use.rst


.. include:: ./commands/dir.rst

.. include:: ./commands/dir_add.rst

| 

.. command:: disto
   :scriptable: 0


.. include:: ./commands/disto_use.rst


.. include:: ./commands/disto.rst

| 

.. command:: dumpheader
   :scriptable: 1


.. include:: ./commands/dumpheader_use.rst


.. include:: ./commands/dumpheader.rst

| 

.. command:: entropy
   :scriptable: 1


.. include:: ./commands/entropy_use.rst


.. include:: ./commands/entropy.rst

| 

.. command:: epf
   :scriptable: 1


.. include:: ./commands/epf_use.rst


.. include:: ./commands/epf.rst

| 

.. command:: exit
   :scriptable: 1


.. include:: ./commands/exit_use.rst


.. include:: ./commands/exit.rst

| 

.. command:: extract
   :scriptable: 1


.. include:: ./commands/extract_use.rst


.. include:: ./commands/extract.rst

| 

.. command:: extract_Green
   :scriptable: 1


.. include:: ./commands/extract_Green_use.rst


.. include:: ./commands/extract_Green.rst

| 

.. command:: extract_Ha
   :scriptable: 1


.. include:: ./commands/extract_Ha_use.rst


.. include:: ./commands/extract_Ha.rst

| 

.. command:: extract_HaOIII
   :scriptable: 1


.. include:: ./commands/extract_HaOIII_use.rst


.. include:: ./commands/extract_HaOIII.rst

| 

.. command:: fdiv
   :scriptable: 1


.. include:: ./commands/fdiv_use.rst


.. include:: ./commands/fdiv.rst

| 

.. command:: ffill
   :scriptable: 1


.. include:: ./commands/ffill_use.rst


.. include:: ./commands/ffill.rst

| 

.. command:: fftd
   :scriptable: 1


.. include:: ./commands/fftd_use.rst


.. include:: ./commands/fftd.rst

| 

.. command:: ffti
   :scriptable: 1


.. include:: ./commands/ffti_use.rst


.. include:: ./commands/ffti.rst

| 

.. command:: fill
   :scriptable: 1


.. include:: ./commands/fill_use.rst


.. include:: ./commands/fill.rst

| 

.. command:: find_cosme
   :scriptable: 1


.. include:: ./commands/find_cosme_use.rst


.. include:: ./commands/find_cosme.rst

| 

.. command:: find_cosme_cfa
   :scriptable: 1


.. include:: ./commands/find_cosme_cfa_use.rst


.. include:: ./commands/find_cosme_cfa.rst

| 

.. command:: find_hot
   :scriptable: 1


.. include:: ./commands/find_hot_use.rst


.. include:: ./commands/find_hot.rst

.. include:: ./commands/find_hot_add.rst

| 

.. command:: findcompstars
   :scriptable: 1


.. include:: ./commands/findcompstars_use.rst


.. include:: ./commands/findcompstars.rst

| 

.. command:: findstar
   :scriptable: 1


.. include:: ./commands/findstar_use.rst


.. include:: ./commands/findstar.rst

| 

.. command:: fix_xtrans
   :scriptable: 1


.. include:: ./commands/fix_xtrans_use.rst


.. include:: ./commands/fix_xtrans.rst

| 

.. command:: fixbanding
   :scriptable: 1


.. include:: ./commands/fixbanding_use.rst


.. include:: ./commands/fixbanding.rst

| 

.. command:: fmedian
   :scriptable: 1


.. include:: ./commands/fmedian_use.rst


.. include:: ./commands/fmedian.rst

| 

.. command:: fmul
   :scriptable: 1


.. include:: ./commands/fmul_use.rst


.. include:: ./commands/fmul.rst

| 

.. command:: gauss
   :scriptable: 1


.. include:: ./commands/gauss_use.rst


.. include:: ./commands/gauss.rst

| 

.. command:: get
   :scriptable: 1


.. include:: ./commands/get_use.rst


.. include:: ./commands/get.rst

| 

.. command:: getref
   :scriptable: 1


.. include:: ./commands/getref_use.rst


.. include:: ./commands/getref.rst

| 

.. command:: ght
   :scriptable: 1


.. include:: ./commands/ght_use.rst


.. include:: ./commands/ght.rst

| 

.. command:: graxpert_bg
   :scriptable: 1


.. include:: ./commands/graxpert_bg_use.rst


.. include:: ./commands/graxpert_bg.rst

| 

.. command:: graxpert_deconv
   :scriptable: 1


.. include:: ./commands/graxpert_deconv_use.rst


.. include:: ./commands/graxpert_deconv.rst

| 

.. command:: graxpert_denoise
   :scriptable: 1


.. include:: ./commands/graxpert_denoise_use.rst


.. include:: ./commands/graxpert_denoise.rst

| 

.. command:: grey_flat
   :scriptable: 1


.. include:: ./commands/grey_flat_use.rst


.. include:: ./commands/grey_flat.rst

| 

.. command:: help
   :scriptable: 1


.. include:: ./commands/help_use.rst


.. include:: ./commands/help.rst

| 

.. command:: histo
   :scriptable: 1


.. include:: ./commands/histo_use.rst


.. include:: ./commands/histo.rst

| 

.. command:: iadd
   :scriptable: 1


.. include:: ./commands/iadd_use.rst


.. include:: ./commands/iadd.rst

| 

.. command:: icc_assign
   :scriptable: 1


.. include:: ./commands/icc_assign_use.rst


.. include:: ./commands/icc_assign.rst

| 

.. command:: icc_convert_to
   :scriptable: 1


.. include:: ./commands/icc_convert_to_use.rst


.. include:: ./commands/icc_convert_to.rst

| 

.. command:: icc_remove
   :scriptable: 1


.. include:: ./commands/icc_remove_use.rst


.. include:: ./commands/icc_remove.rst

| 

.. command:: idiv
   :scriptable: 1


.. include:: ./commands/idiv_use.rst


.. include:: ./commands/idiv.rst

| 

.. command:: imul
   :scriptable: 1


.. include:: ./commands/imul_use.rst


.. include:: ./commands/imul.rst

| 

.. command:: inspector
   :scriptable: 0


.. include:: ./commands/inspector_use.rst


.. include:: ./commands/inspector.rst

| 

.. command:: invght
   :scriptable: 1


.. include:: ./commands/invght_use.rst


.. include:: ./commands/invght.rst

| 

.. command:: invmodasinh
   :scriptable: 1


.. include:: ./commands/invmodasinh_use.rst


.. include:: ./commands/invmodasinh.rst

| 

.. command:: invmtf
   :scriptable: 1


.. include:: ./commands/invmtf_use.rst


.. include:: ./commands/invmtf.rst

| 

.. command:: isub
   :scriptable: 1


.. include:: ./commands/isub_use.rst


.. include:: ./commands/isub.rst

| 

.. command:: jsonmetadata
   :scriptable: 1


.. include:: ./commands/jsonmetadata_use.rst


.. include:: ./commands/jsonmetadata.rst

| 

.. command:: light_curve
   :scriptable: 1


.. include:: ./commands/light_curve_use.rst


.. include:: ./commands/light_curve.rst

| 

.. command:: limit
   :scriptable: 1


.. include:: ./commands/limit_use.rst


.. include:: ./commands/limit.rst

| 

.. command:: linear_match
   :scriptable: 1


.. include:: ./commands/linear_match_use.rst


.. include:: ./commands/linear_match.rst

| 

.. command:: link
   :scriptable: 1


.. include:: ./commands/link_use.rst


.. include:: ./commands/link.rst

| 

.. command:: linstretch
   :scriptable: 1


.. include:: ./commands/linstretch_use.rst


.. include:: ./commands/linstretch.rst

| 

.. command:: livestack
   :scriptable: 1


.. include:: ./commands/livestack_use.rst


.. include:: ./commands/livestack.rst

.. include:: ./commands/livestack_add.rst

| 

.. command:: load
   :scriptable: 1


.. include:: ./commands/load_use.rst


.. include:: ./commands/load.rst

| 

.. command:: load_seq
   :scriptable: 0


.. include:: ./commands/load_seq_use.rst


.. include:: ./commands/load_seq.rst

| 

.. command:: log
   :scriptable: 1


.. include:: ./commands/log_use.rst


.. include:: ./commands/log.rst

| 

.. command:: ls
   :scriptable: 0


.. include:: ./commands/ls_use.rst


.. include:: ./commands/ls.rst

.. include:: ./commands/ls_add.rst

| 

.. command:: makepsf
   :scriptable: 1


.. include:: ./commands/makepsf_use.rst


.. include:: ./commands/makepsf.rst

| 

.. command:: merge
   :scriptable: 1


.. include:: ./commands/merge_use.rst


.. include:: ./commands/merge.rst

| 

.. command:: merge_cfa
   :scriptable: 1


.. include:: ./commands/merge_cfa_use.rst


.. include:: ./commands/merge_cfa.rst

| 

.. command:: mirrorx
   :scriptable: 1


.. include:: ./commands/mirrorx_use.rst


.. include:: ./commands/mirrorx.rst

| 

.. command:: mirrorx_single
   :scriptable: 1


.. include:: ./commands/mirrorx_single_use.rst


.. include:: ./commands/mirrorx_single.rst

| 

.. command:: mirrory
   :scriptable: 1


.. include:: ./commands/mirrory_use.rst


.. include:: ./commands/mirrory.rst

| 

.. command:: modasinh
   :scriptable: 1


.. include:: ./commands/modasinh_use.rst


.. include:: ./commands/modasinh.rst

| 

.. command:: mtf
   :scriptable: 1


.. include:: ./commands/mtf_use.rst


.. include:: ./commands/mtf.rst

| 

.. command:: neg
   :scriptable: 1


.. include:: ./commands/neg_use.rst


.. include:: ./commands/neg.rst

| 

.. command:: new
   :scriptable: 0


.. include:: ./commands/new_use.rst


.. include:: ./commands/new.rst

| 

.. command:: nozero
   :scriptable: 1


.. include:: ./commands/nozero_use.rst


.. include:: ./commands/nozero.rst

| 

.. command:: offline
   :scriptable: 1


.. include:: ./commands/offline_use.rst


.. include:: ./commands/offline.rst

| 

.. command:: offset
   :scriptable: 1


.. include:: ./commands/offset_use.rst


.. include:: ./commands/offset.rst

| 

.. command:: online
   :scriptable: 1


.. include:: ./commands/online_use.rst


.. include:: ./commands/online.rst

| 

.. command:: parse
   :scriptable: 1


.. include:: ./commands/parse_use.rst


.. include:: ./commands/parse.rst

| 

.. command:: pcc
   :scriptable: 1


.. include:: ./commands/pcc_use.rst


.. include:: ./commands/pcc.rst

| 

.. command:: platesolve
   :scriptable: 1


.. include:: ./commands/platesolve_use.rst


.. include:: ./commands/platesolve.rst

| 

.. command:: pm
   :scriptable: 1


.. include:: ./commands/pm_use.rst


.. include:: ./commands/pm.rst

| 

.. command:: profile
   :scriptable: 1


.. include:: ./commands/profile_use.rst


.. include:: ./commands/profile.rst

| 

.. command:: psf
   :scriptable: 1


.. include:: ./commands/psf_use.rst


.. include:: ./commands/psf.rst

| 

.. command:: pwd
   :scriptable: 1


.. include:: ./commands/pwd_use.rst


.. include:: ./commands/pwd.rst

| 

.. command:: pyscript
   :scriptable: 1


.. include:: ./commands/pyscript_use.rst


.. include:: ./commands/pyscript.rst

| 

.. command:: register
   :scriptable: 1


.. include:: ./commands/register_use.rst


.. include:: ./commands/register.rst

| 

.. command:: reloadscripts
   :scriptable: 0


.. include:: ./commands/reloadscripts_use.rst


.. include:: ./commands/reloadscripts.rst

| 

.. command:: requires
   :scriptable: 1


.. include:: ./commands/requires_use.rst


.. include:: ./commands/requires.rst

| 

.. command:: resample
   :scriptable: 1


.. include:: ./commands/resample_use.rst


.. include:: ./commands/resample.rst

| 

.. command:: rgbcomp
   :scriptable: 1


.. include:: ./commands/rgbcomp_use.rst


.. include:: ./commands/rgbcomp.rst

| 

.. command:: rgradient
   :scriptable: 1


.. include:: ./commands/rgradient_use.rst


.. include:: ./commands/rgradient.rst

| 

.. command:: rl
   :scriptable: 1


.. include:: ./commands/rl_use.rst


.. include:: ./commands/rl.rst

| 

.. command:: rmgreen
   :scriptable: 1


.. include:: ./commands/rmgreen_use.rst


.. include:: ./commands/rmgreen.rst

| 

.. command:: rotate
   :scriptable: 1


.. include:: ./commands/rotate_use.rst


.. include:: ./commands/rotate.rst

| 

.. command:: rotatePi
   :scriptable: 1


.. include:: ./commands/rotatePi_use.rst


.. include:: ./commands/rotatePi.rst

| 

.. command:: satu
   :scriptable: 1


.. include:: ./commands/satu_use.rst


.. include:: ./commands/satu.rst

| 

.. command:: save
   :scriptable: 1


.. include:: ./commands/save_use.rst


.. include:: ./commands/save.rst

| 

.. command:: savebmp
   :scriptable: 1


.. include:: ./commands/savebmp_use.rst


.. include:: ./commands/savebmp.rst

| 

.. command:: savejpg
   :scriptable: 1


.. include:: ./commands/savejpg_use.rst


.. include:: ./commands/savejpg.rst

| 

.. command:: savejxl
   :scriptable: 1


.. include:: ./commands/savejxl_use.rst


.. include:: ./commands/savejxl.rst

| 

.. command:: savepng
   :scriptable: 1


.. include:: ./commands/savepng_use.rst


.. include:: ./commands/savepng.rst

| 

.. command:: savepnm
   :scriptable: 1


.. include:: ./commands/savepnm_use.rst


.. include:: ./commands/savepnm.rst

| 

.. command:: savetif
   :scriptable: 1


.. include:: ./commands/savetif_use.rst


.. include:: ./commands/savetif.rst

| 

.. command:: savetif32
   :scriptable: 1


.. include:: ./commands/savetif32_use.rst


.. include:: ./commands/savetif32.rst

| 

.. command:: savetif8
   :scriptable: 1


.. include:: ./commands/savetif8_use.rst


.. include:: ./commands/savetif8.rst

| 

.. command:: sb
   :scriptable: 1


.. include:: ./commands/sb_use.rst


.. include:: ./commands/sb.rst

| 

.. command:: select
   :scriptable: 1


.. include:: ./commands/select_use.rst


.. include:: ./commands/select.rst

.. include:: ./commands/select_add.rst

| 

.. command:: seqapplyreg
   :scriptable: 1


.. include:: ./commands/seqapplyreg_use.rst


.. include:: ./commands/seqapplyreg.rst

.. include:: ./commands/seqapplyreg_add.rst

| 

.. command:: seqccm
   :scriptable: 1


.. include:: ./commands/seqccm_use.rst


.. include:: ./commands/seqccm.rst

| 

.. command:: seqclean
   :scriptable: 1


.. include:: ./commands/seqclean_use.rst


.. include:: ./commands/seqclean.rst

| 

.. command:: seqcosme
   :scriptable: 1


.. include:: ./commands/seqcosme_use.rst


.. include:: ./commands/seqcosme.rst

| 

.. command:: seqcosme_cfa
   :scriptable: 1


.. include:: ./commands/seqcosme_cfa_use.rst


.. include:: ./commands/seqcosme_cfa.rst

| 

.. command:: seqcrop
   :scriptable: 1


.. include:: ./commands/seqcrop_use.rst


.. include:: ./commands/seqcrop.rst

| 

.. command:: seqextract_Green
   :scriptable: 1


.. include:: ./commands/seqextract_Green_use.rst


.. include:: ./commands/seqextract_Green.rst

| 

.. command:: seqextract_Ha
   :scriptable: 1


.. include:: ./commands/seqextract_Ha_use.rst


.. include:: ./commands/seqextract_Ha.rst

| 

.. command:: seqextract_HaOIII
   :scriptable: 1


.. include:: ./commands/seqextract_HaOIII_use.rst


.. include:: ./commands/seqextract_HaOIII.rst

| 

.. command:: seqfind_cosme
   :scriptable: 1


.. include:: ./commands/seqfind_cosme_use.rst


.. include:: ./commands/seqfind_cosme.rst

| 

.. command:: seqfind_cosme_cfa
   :scriptable: 1


.. include:: ./commands/seqfind_cosme_cfa_use.rst


.. include:: ./commands/seqfind_cosme_cfa.rst

| 

.. command:: seqfindstar
   :scriptable: 1


.. include:: ./commands/seqfindstar_use.rst


.. include:: ./commands/seqfindstar.rst

| 

.. command:: seqfixbanding
   :scriptable: 1


.. include:: ./commands/seqfixbanding_use.rst


.. include:: ./commands/seqfixbanding.rst

| 

.. command:: seqght
   :scriptable: 1


.. include:: ./commands/seqght_use.rst


.. include:: ./commands/seqght.rst

| 

.. command:: seqgraxpert_bg
   :scriptable: 1


.. include:: ./commands/seqgraxpert_bg_use.rst


.. include:: ./commands/seqgraxpert_bg.rst

| 

.. command:: seqgraxpert_deconv
   :scriptable: 1


.. include:: ./commands/seqgraxpert_deconv_use.rst


.. include:: ./commands/seqgraxpert_deconv.rst

| 

.. command:: seqgraxpert_denoise
   :scriptable: 1


.. include:: ./commands/seqgraxpert_denoise_use.rst


.. include:: ./commands/seqgraxpert_denoise.rst

| 

.. command:: seqheader
   :scriptable: 1


.. include:: ./commands/seqheader_use.rst


.. include:: ./commands/seqheader.rst

| 

.. command:: seqinvght
   :scriptable: 1


.. include:: ./commands/seqinvght_use.rst


.. include:: ./commands/seqinvght.rst

| 

.. command:: seqinvmodasinh
   :scriptable: 1


.. include:: ./commands/seqinvmodasinh_use.rst


.. include:: ./commands/seqinvmodasinh.rst

| 

.. command:: seqlinstretch
   :scriptable: 1


.. include:: ./commands/seqlinstretch_use.rst


.. include:: ./commands/seqlinstretch.rst

| 

.. command:: seqmerge_cfa
   :scriptable: 1


.. include:: ./commands/seqmerge_cfa_use.rst


.. include:: ./commands/seqmerge_cfa.rst

| 

.. command:: seqmodasinh
   :scriptable: 1


.. include:: ./commands/seqmodasinh_use.rst


.. include:: ./commands/seqmodasinh.rst

| 

.. command:: seqmtf
   :scriptable: 1


.. include:: ./commands/seqmtf_use.rst


.. include:: ./commands/seqmtf.rst

| 

.. command:: seqprofile
   :scriptable: 1


.. include:: ./commands/seqprofile_use.rst


.. include:: ./commands/seqprofile.rst

| 

.. command:: seqpsf
   :scriptable: 1


.. include:: ./commands/seqpsf_use.rst


.. include:: ./commands/seqpsf.rst

| 

.. command:: seqplatesolve
   :scriptable: 1


.. include:: ./commands/seqplatesolve_use.rst


.. include:: ./commands/seqplatesolve.rst

| 

.. command:: seqresample
   :scriptable: 1


.. include:: ./commands/seqresample_use.rst


.. include:: ./commands/seqresample.rst

| 

.. command:: seqrl
   :scriptable: 1


.. include:: ./commands/seqrl_use.rst


.. include:: ./commands/seqrl.rst

| 

.. command:: seqsb
   :scriptable: 1


.. include:: ./commands/seqsb_use.rst


.. include:: ./commands/seqsb.rst

| 

.. command:: seqsplit_cfa
   :scriptable: 1


.. include:: ./commands/seqsplit_cfa_use.rst


.. include:: ./commands/seqsplit_cfa.rst

| 

.. command:: seqstarnet
   :scriptable: 1


.. include:: ./commands/seqstarnet_use.rst


.. include:: ./commands/seqstarnet.rst

| 

.. command:: seqstat
   :scriptable: 1


.. include:: ./commands/seqstat_use.rst


.. include:: ./commands/seqstat.rst

| 

.. command:: seqsubsky
   :scriptable: 1


.. include:: ./commands/seqsubsky_use.rst


.. include:: ./commands/seqsubsky.rst

| 

.. command:: seqtilt
   :scriptable: 1


.. include:: ./commands/seqtilt_use.rst


.. include:: ./commands/seqtilt.rst

| 

.. command:: sequnsetmag
   :scriptable: 0


.. include:: ./commands/sequnsetmag_use.rst


.. include:: ./commands/sequnsetmag.rst

| 

.. command:: sequpdate_key
   :scriptable: 1


.. include:: ./commands/sequpdate_key_use.rst


.. include:: ./commands/sequpdate_key.rst

| 

.. command:: seqwiener
   :scriptable: 1


.. include:: ./commands/seqwiener_use.rst


.. include:: ./commands/seqwiener.rst

| 

.. command:: set
   :scriptable: 1


.. include:: ./commands/set_use.rst


.. include:: ./commands/set.rst

| 

.. command:: set16bits
   :scriptable: 1


.. include:: ./commands/set16bits_use.rst


.. include:: ./commands/set16bits.rst

| 

.. command:: set32bits
   :scriptable: 1


.. include:: ./commands/set32bits_use.rst


.. include:: ./commands/set32bits.rst

| 

.. command:: setcompress
   :scriptable: 1


.. include:: ./commands/setcompress_use.rst


.. include:: ./commands/setcompress.rst

| 

.. command:: setcpu
   :scriptable: 1


.. include:: ./commands/setcpu_use.rst


.. include:: ./commands/setcpu.rst

| 

.. command:: setext
   :scriptable: 1


.. include:: ./commands/setext_use.rst


.. include:: ./commands/setext.rst

| 

.. command:: setfindstar
   :scriptable: 1


.. include:: ./commands/setfindstar_use.rst


.. include:: ./commands/setfindstar.rst

.. include:: ./commands/setfindstar_add.rst

| 

.. command:: setmag
   :scriptable: 0


.. include:: ./commands/setmag_use.rst


.. include:: ./commands/setmag.rst

| 

.. command:: seqsetmag
   :scriptable: 0


.. include:: ./commands/seqsetmag_use.rst


.. include:: ./commands/seqsetmag.rst

| 

.. command:: setmem
   :scriptable: 1


.. include:: ./commands/setmem_use.rst


.. include:: ./commands/setmem.rst

| 

.. command:: setphot
   :scriptable: 1


.. include:: ./commands/setphot_use.rst


.. include:: ./commands/setphot.rst

| 

.. command:: setref
   :scriptable: 1


.. include:: ./commands/setref_use.rst


.. include:: ./commands/setref.rst

| 

.. command:: show
   :scriptable: 0


.. include:: ./commands/show_use.rst


.. include:: ./commands/show.rst

| 

.. command:: spcc
   :scriptable: 1


.. include:: ./commands/spcc_use.rst


.. include:: ./commands/spcc.rst

| 

.. command:: spcc_list
   :scriptable: 1


.. include:: ./commands/spcc_list_use.rst


.. include:: ./commands/spcc_list.rst

| 

.. command:: split
   :scriptable: 1


.. include:: ./commands/split_use.rst


.. include:: ./commands/split.rst

| 

.. command:: split_cfa
   :scriptable: 1


.. include:: ./commands/split_cfa_use.rst


.. include:: ./commands/split_cfa.rst

| 

.. command:: stack
   :scriptable: 1


.. include:: ./commands/stack_use.rst


.. include:: ./commands/stack.rst

.. include:: ./commands/stack_add.rst

| 

.. command:: stackall
   :scriptable: 1


.. include:: ./commands/stackall_use.rst


.. include:: ./commands/stackall.rst

| 

.. command:: starnet
   :scriptable: 1


.. include:: ./commands/starnet_use.rst


.. include:: ./commands/starnet.rst

| 

.. command:: start_ls
   :scriptable: 1


.. include:: ./commands/start_ls_use.rst


.. include:: ./commands/start_ls.rst

| 

.. command:: stat
   :scriptable: 1


.. include:: ./commands/stat_use.rst


.. include:: ./commands/stat.rst

| 

.. command:: stop_ls
   :scriptable: 1


.. include:: ./commands/stop_ls_use.rst


.. include:: ./commands/stop_ls.rst

| 

.. command:: subsky
   :scriptable: 1


.. include:: ./commands/subsky_use.rst


.. include:: ./commands/subsky.rst

| 

.. command:: synthstar
   :scriptable: 1


.. include:: ./commands/synthstar_use.rst


.. include:: ./commands/synthstar.rst

| 

.. command:: threshlo
   :scriptable: 1


.. include:: ./commands/threshlo_use.rst


.. include:: ./commands/threshlo.rst

| 

.. command:: threshhi
   :scriptable: 1


.. include:: ./commands/threshhi_use.rst


.. include:: ./commands/threshhi.rst

| 

.. command:: thresh
   :scriptable: 1


.. include:: ./commands/thresh_use.rst


.. include:: ./commands/thresh.rst

| 

.. command:: tilt
   :scriptable: 0


.. include:: ./commands/tilt_use.rst


.. include:: ./commands/tilt.rst

| 

.. command:: trixel
   :scriptable: 1


.. include:: ./commands/trixel_use.rst


.. include:: ./commands/trixel.rst

| 

.. command:: unclipstars
   :scriptable: 1


.. include:: ./commands/unclipstars_use.rst


.. include:: ./commands/unclipstars.rst

| 

.. command:: unpurple
   :scriptable: 1


.. include:: ./commands/unpurple_use.rst


.. include:: ./commands/unpurple.rst

| 

.. command:: unselect
   :scriptable: 1


.. include:: ./commands/unselect_use.rst


.. include:: ./commands/unselect.rst

| 

.. command:: unsetmag
   :scriptable: 0


.. include:: ./commands/unsetmag_use.rst


.. include:: ./commands/unsetmag.rst

| 

.. command:: unsharp
   :scriptable: 1


.. include:: ./commands/unsharp_use.rst


.. include:: ./commands/unsharp.rst

| 

.. command:: update_key
   :scriptable: 1


.. include:: ./commands/update_key_use.rst


.. include:: ./commands/update_key.rst

| 

.. command:: visu
   :scriptable: 0


.. include:: ./commands/visu_use.rst


.. include:: ./commands/visu.rst

| 

.. command:: wavelet
   :scriptable: 1


.. include:: ./commands/wavelet_use.rst


.. include:: ./commands/wavelet.rst

| 

.. command:: wiener
   :scriptable: 1


.. include:: ./commands/wiener_use.rst


.. include:: ./commands/wiener.rst

| 

.. command:: wrecons
   :scriptable: 1


.. include:: ./commands/wrecons_use.rst


.. include:: ./commands/wrecons.rst

| 
