Burger Menu
===========

.. image:: ../_images/GUI/main-interface_mainmenu1.png
   :alt: Main Menu Page 1

.. rubric:: Preferences

Opens the :ref:`Preferences <preferences:preferences>` menu.

.. rubric:: Siril Manual

Opens the online `documentation <https://siril.readthedocs.io>`_.

.. rubric:: Check for updates

Checks if a newer version is available.

.. rubric:: Get scripts

Opens the :ref:`preference scripts tab <preferences/preferences_gui:Scripts>` to install more scripts than the ones which are shipped with Siril.

.. rubric:: Keyboards shortcuts

Opens a panel reminding all the available :ref:`gui/shortcuts:shortcuts`.

.. rubric:: About Siril

Opens a dialog showing te version information and Credits.
