Tools Menu
==========

.. figure:: ../_images/GUI/tools-menu.png
    :alt: tools menu
    :class: with-shadow

    Tools menu
    
Introduced with version 1.4, this menu brings together Siril's various tools, 
such as image analysis, astrometry, photometry, *etc*.

It consists of several nested menus and sub-menus, organized as follows:

- Image Analysis
    - :ref:`Statistics <Statistics:Statistics>`
    - :ref:`Noise Estimation <Statistics:Background noise>`
    - :ref:`Aberration Inspector <Image-inspection:Aberration Inspector>`
    - :ref:`Show Tilt <Image-inspection:Tilt>`
    - :ref:`Show Distortions <astrometry/platesolving:Visualizing distortions>`
    - :ref:`Dynamic PSF <Dynamic-PSF:Dynamic PSF>`
- Astrometry
    - :ref:`Image Plate Solver <astrometry/platesolving:Platesolving>`
    - :ref:`Annotate <astrometry/annotations:Annotations>`
- Photometry
    - :ref:`Create Comparison Star File <photometry/lightcurves:Generating a list of comparison stars>`
    - :ref:`Automated Light Curve <photometry/lightcurves:Automated Light Curve>`
    - Create Output
        - :ref:`Light Curve (ETD format) <photometry/lightcurves:Light Curve (ETD format)>`
        - :ref:`AAVSO file format <photometry/lightcurves:AAVSO extended file format>`
- :ref:`Color Management <Color-management:Color Management>`
- :ref:`FITS Header <FITS-header:FITS header>`
- :ref:`Image Information <GUI/information_window:Image information window>`

For detailed information on each item, click on the respective entry.
