.. code-block:: text

    seqgraxpert_denoise sequencename [-strength=] [ { -gpu | -cpu } ]