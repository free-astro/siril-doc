.. code-block:: text

    seqapplyreg sequencename [-prefix=] [-scale=] [-layer=] [-framing=]
    seqapplyreg sequencename ... [-interp=] [-noclamp]
    seqapplyreg sequencename ... [-drizzle [-pixfrac=] [-kernel=] [-flat=]]
    seqapplyreg sequencename ... [-filter-fwhm=value[%|k]] [-filter-wfwhm=value[%|k]] [-filter-round=value[%|k]] [-filter-bkg=value[%|k]] [-filter-nbstars=value[%|k]] [-filter-quality=value[%|k]] [-filter-incl[uded]]