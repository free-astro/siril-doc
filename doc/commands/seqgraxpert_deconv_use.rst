.. code-block:: text

    seqgraxpert_deconv sequencename [-strength=] [-psfsize=] [ { -gpu | -cpu } ]