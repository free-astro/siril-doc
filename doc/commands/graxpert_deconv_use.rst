.. code-block:: text

    graxpert_deconv { object | stellar } [-strength=] [-psfsize=] [ { -gpu | -cpu } [-ai_version=] ]