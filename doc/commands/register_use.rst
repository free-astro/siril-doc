.. code-block:: text

    register sequencename [-2pass] [-selected] [-prefix=] [-scale=]
    register sequencename ... [-layer=] [-transf=] [-minpairs=] [-maxstars=] [-nostarlist] [-disto=]
    register sequencename ... [-interp=] [-noclamp]
    register sequencename ... [-drizzle [-pixfrac=] [-kernel=] [-flat=]]