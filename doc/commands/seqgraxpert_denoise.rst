| Applies the external GraXpert program to a sequence, in denoising mode. The first argument must be the sequence name; the remaining arguments are the same as for the GRAXPERT_DENOISE command
| 
| Links: :ref:`graxpert_denoise <graxpert_denoise>`
