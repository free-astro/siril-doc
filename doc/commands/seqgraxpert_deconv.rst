| Applies the external GraXpert program to a sequence, in deconvolution mode. The first argument must be the sequence name; the remaining arguments are the same as for the GRAXPERT_DECONV command
| 
| Links: :ref:`graxpert_deconv <graxpert_deconv>`
