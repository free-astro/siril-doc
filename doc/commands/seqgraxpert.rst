| Applies the external GraXpert program to a sequence. The first argument must be the sequence name; the remaining arguments are the same as for the **graxpert** command
