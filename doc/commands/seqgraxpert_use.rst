.. code-block:: text

    graxpert sequencename { -bg | -denoise } [-algo=] [-mode=] [-kernel=] [-ai_batch_size=] [-pts_per_row=] [-splineorder=] [-samplesize=] [-smoothing=] [-bgtol=] [-strength=] [ { -gpu | -cpu } ] [-keep_bg]