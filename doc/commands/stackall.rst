| Opens all sequences in the current directory and stacks them with the optionally specified stacking type and filtering or with sum stacking. See STACK command for options description
| 
| Links: :ref:`stack <stack>`
