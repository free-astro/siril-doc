| Same command as UPDATE_KEY but for the sequence **sequencename**. However, this command won't work on SER sequence
| 
| Links: :ref:`update_key <update_key>`
