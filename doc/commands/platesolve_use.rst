.. code-block:: text

    platesolve [-force] [image_center_coords] [-focal=] [-pixelsize=]
    platesolve sequencename ... [-noflip] [-downscale] [-order=] [-radius=] [-disto=]
    platesolve sequencename ... [-limitmag=[+-]] [-catalog=] [-nocrop]
    platesolve sequencename ... [-localasnet [-blindpos] [-blindres]]