.. code-block:: text

    seqplatesolve sequencename [image_center_coords] [-focal=] [-pixelsize=]
    seqplatesolve sequencename ... [-downscale] [-order=] [-radius=] [-force] [-noreg] [-disto=]
    seqplatesolve sequencename ... [-limitmag=[+-]] [-catalog=] [-nocrop] [-nocache]
    seqplatesolve sequencename ... [-localasnet [-blindpos] [-blindres]]