|
| Examples:
| 
| `select . 0 0`
| selects the first of the currently loaded sequence
| 
| `select sequencename 1000 1200`
| selects 201 images starting from number 1000 in sequence named sequencename
| 
| The second number can be greater than the number of images to just go up to the end.

