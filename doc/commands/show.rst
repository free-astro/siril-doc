| Shows a point on the loaded plate solved image using the temporary user annotations catalogue, based on its equatorial coordinates. The **-clear** option clears this catalogue first and can be used alone.
| Several points can be passed using a CSV file with the option **-list=** containing at least ra and dec columns. If the passed file also contains a column name, names will be used as tags in the image and listed in the Console, unless toggled off with the options **-notag** and **-nolog**.
| 
| This is only available from the GUI of Siril
