.. code-block:: text

    seqapplydrizzle sequencename [-framing=] [-scale=] [-pixfrac=] [-kernel=] [-flat=] [-bias=] [-prefix=] [-filter-fwhm=value[%|k]] [-filter-wfwhm=value[%|k]] [-filter-round=value[%|k]] [-filter-bkg=value[%|k]] [-filter-nbstars=value[%|k]] [-filter-quality=value[%|k]] [-filter-incl[uded]]