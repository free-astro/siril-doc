.. code-block:: text

    resample { factor | -width= | -height= | -maxdim= } [-interp=] [-noclamp]