| Restores an image using the Wiener deconvolution method.
| 
| Optionally, a PSF created by MAKEPSF may be loaded using the argument **-loadpsf=\ filename**.
| 
| The parameter **-alpha=** provides the Gaussian noise modelled regularization factor
| 
| Links: :ref:`psf <psf>`, :ref:`makepsf <makepsf>`
