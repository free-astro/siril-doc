| Allows easy mass unselection of images in the sequence **sequencename** (from **from** to **to** included). See SELECT
| 
| Links: :ref:`select <select>`
