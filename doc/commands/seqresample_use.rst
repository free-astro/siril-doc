.. code-block:: text

    seqresample sequencename { -scale= | -width= | -height= } [-interp=] [-prefix=]