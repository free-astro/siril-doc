| Applies the external GraXpert program to a sequence, in background extraction mode. The first argument must be the sequence name; the remaining arguments are the same as for the GRAXPERT_BG command
| 
| Links: :ref:`graxpert_bg <graxpert_bg>`
