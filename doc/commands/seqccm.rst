| Same command as CCM but for the the sequence **sequencename**. Only selected images in the sequence are processed.
| 
| The output sequence name starts with the prefix "ccm" unless otherwise specified with option **-prefix=**
| 
| Links: :ref:`ccm <ccm>`
