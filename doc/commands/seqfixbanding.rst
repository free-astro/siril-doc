| Same command as FIXBANDING but for the sequence **sequencename**.
| 
| The output sequence name starts with the prefix "unband\_" unless otherwise specified with **-prefix=** option
| 
| Links: :ref:`fixbanding <fixbanding>`
