.. code-block:: text

    rgbcomp red green blue [-out=result_filename] [-nosum]
    rgbcomp -lum=image { rgb_image | red green blue } [-out=result_filename] [-nosum]