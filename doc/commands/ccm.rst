| Applies a color conversion matrix to the current image.
| 
| There are 9 mandatory arguments corresponding to the 9 matrix elements:
| 
| m00, m01, m02
| m10, m11, m12
| m20, m21, m22
| 
| An additional tenth argument **[gamma]** can be provided: if it is omitted, it defaults to 1.0.
| 
| These are applied to each pixel according to the following formulae:
| 
| r' = (m00 \* r + m01 \* g + m02 \* b)^(-1/gamma)
| g' = (m10 \* r + m11 \* g + m12 \* b)^(-1/gamma)
| b' = (m20 \* r + m21 \* g + m22 \* b)^(-1/gamma)
