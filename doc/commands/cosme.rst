| Applies the local mean to a set of pixels on the loaded image (cosmetic correction). The coordinates of these pixels are in a text file [.lst file], the FIND_HOT command can also create it for single hot pixels, but manual operation is needed to remove rows or columns. COSME is adapted to correct residual hot and cold pixels after calibration.
| Instead of providing the list of bad pixels, it's also possible to detect them in the current image using the FIND_COSME command
| 
| Links: :ref:`find_hot <find_hot>`, :ref:`find_cosme <find_cosme>`
