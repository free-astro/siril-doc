| Gets a value from the settings using its name, or list all with **-a** (name and value list) or with **-A** (detailed list)
| 
| See also SET to update values
| 
| Links: :ref:`set <set>`
