.. code-block:: text

    sequpdate_key sequencename key value [keycomment]
    sequpdate_key sequencename -delete key
    sequpdate_key sequencename -modify key newkey
    sequpdate_key sequencename -comment comment