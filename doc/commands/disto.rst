| Shows distortion field on a plate-solved image which solution includes distortion terms
| 
| Pass option **clear** to disable
