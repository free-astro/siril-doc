.. code-block:: text

    update_key key value [keycomment]
    update_key -delete key
    update_key -modify key newkey
    update_key -comment comment