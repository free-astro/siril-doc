import pathlib
import subprocess
from pathlib import Path
from subprocess import Popen, PIPE, STDOUT
import sys
import os
import shutil
import urllib.parse

def get_active_branch_name():
    """
    Get branch name for a specific commit using git CLI.

    Returns:
        str: Branch name or None if not found
    """
    # Debug: Print all environment variables
    print("DEBUG: All environment variables:")
    for key, value in os.environ.items():
        print(f"{key}: {value}")
    print("END DEBUG\n")

    clone_url = os.environ.get('READTHEDOCS_GIT_CLONE_URL')
    commit = os.environ.get('READTHEDOCS_GIT_COMMIT_HASH')
    cwd = os.getcwd()
    if not (clone_url and commit):
        return None

    try:
        # Temporary directory for clone
        import tempfile
        with tempfile.TemporaryDirectory() as tmpdir:
            # Clone repository
            subprocess.run(['git', 'clone', clone_url, tmpdir],
                           check=True, capture_output=True)

            # Change to temp directory
            os.chdir(tmpdir)

            # Find branches containing the commit
            result = subprocess.run(
                ['git', 'branch', '-r', '--contains', commit],
                capture_output=True,
                text=True
            )

            # Extract branch names, remove remote prefix
            branches = [
                branch.strip().split('/')[-1]
                for branch in result.stdout.splitlines()
            ]

            # Convert 'main' to 'master' if needed
            return 'master' if 'main' in branches else (branches[0] if branches else None)

    except subprocess.CalledProcessError:
        return None
    finally:
        os.chdir(cwd)
        print(f"Changed dir to: {cwd}")


def get_repo_url():
    """Get repository URL and convert to HTTPS if needed."""
    p = Popen('git config --get remote.origin.url', stdout=PIPE, stdin=PIPE, stderr=STDOUT, shell=True)
    url = p.communicate()[0].decode().strip()

    if url.startswith('git@'):
        url = url.replace(':', '/').replace('git@gitlab.com/', 'https://gitlab.com/')

    if url.endswith('.git'):
        url = url[:-4]
    return url

def append_to_requirements(repo_url, branch):
    """Append repository and branch information to requirements.txt"""
    requirement_line = f"git+{repo_url}@{branch}#egg=sirilpy&subdirectory=python_module"

    try:
        with open('requirements.txt', 'a') as f:
            # Add a newline first in case the file doesn't end with one
            f.write('\n' + requirement_line + '\n')
        print(f"Successfully appended to requirements.txt: {requirement_line}")
    except Exception as e:
        print(f"Error writing to requirements.txt: {str(e)}")
        raise

def main():
    try:
        # Get repository information
        repo_url = get_repo_url()
        repo_url = repo_url.removesuffix("-doc")
        repo_url = repo_url + '.git'
        branch = get_active_branch_name()
        if branch == "main":
            branch = "master"
        if branch is None: # use master as fallback
            branch = "master"
        if branch.startswith("command"):
            branch = "master"

        # Set source and target paths
        source_path = "python_module"
        target_path = "python_module"

        print(f"Repository URL: {repo_url}")
        print(f"Branch: {branch}")
        print(f"Source path: {source_path}")
        print(f"Target path: {target_path}")

        # Download files
        append_to_requirements(repo_url, branch)

    except Exception as e:
        print(f"Error: {str(e)}")
        sys.exit(1)

if __name__ == "__main__":
    main()
